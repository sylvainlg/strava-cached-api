<?php

namespace SylvainLG\Bundle\StravaCacheBundle\Service;

use Iamstuartwilson\StravaApi;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

use Clue\JsonQuery\QueryExpressionFilter;

class CacheStrava {

	const MAX_PER_PAGE = 199;

	private $user;
	private $api;
	private $em;

	private $cacheFilename;
	private $invalidatedCache = false;
	private $cacheFolder;
	private $cacheLifetime;

	private $cacheEnabled = true;

	public function __construct(TokenStorage $ts, EntityManager $em, $params, $cacheFolder, $cacheTime) {

    	$this->user = $ts->getToken()->getUser();
    	$this->em = $em;

    	$this->cacheFolder = $cacheFolder;
    	$this->cacheLifetime = intval($cacheTime);

    	$this->api = new StravaApi($params['strava']['client_id'], $params['strava']['client_secret']);
    	$this->api->setAccessToken($ts->getToken()->getAccessToken());

	}

	/**
	 * Est ce que les fichiers de cache sont présents pour l'utilisateur
	 */
	public function hasCache() {
		$test = glob($this->cacheFolder.$this->user->getUser()->getId().'_*');
		return is_array($test) && count($test) > 0;
	}

	/**
	 * Get cache
	 * 	$key is optionnal
	 * 
	 * return array result
	 */
	public function get($key = null) {

		$result = null;



		switch ($key) {
			case 'bikes':
				$result = ['bikes' => $this->indexify($this->read('bikes'))];
				break;
			
			case 'activities':
				$result = ['activities' => $this->indexify($this->read('activities'))];
				break;

			default:
				$result = [
					'bikes' => $this->indexify($this->read('bikes')),
					'activities' => $this->indexify($this->read('activities')),
				];
				break;
		}

		return $result;

	}

	/**
	 * Get cache with filter
	 */
	public function filter($key, $filter) {

		if(!is_array($filter)) {
			throw new Exception("Filter must be an array", 1);
			
		}

		$filter = new QueryExpressionFilter($filter);

// var_dump($filter);

		$items = $this->get($key)[$key];
		$res = [];

// var_dump(current($items));

		foreach($items as $item) {
			if($filter->doesMatch($item)) {
				$res[] = $item;
			}
		}
// var_dump($res);
		return $res;
	}

	/**
	 * @return List of bikes belong to the current user
	 */
	public function getBikes() {

		if($this->user->getUser()->getSocial() == 'strava') {
			$athlete = $this->api->get('athlete');
			return $athlete->bikes ? $athlete->bikes : [];
		}

		return [];

	}

	public function buildCache($rebuild = true) {

		if( $rebuild || !$this->isCacheValid('bikes')) {
			$list = $this->getBikes();

			$bikes = [];

			foreach ($list as $bike) {
				$details = $this->api->get('gear/'.$bike->id);
				$bikes[] = $details;
			}

			$this->write('bikes', $bikes);
		}
		if( $rebuild || !$this->isCacheValid('activities')) {

			$list = $this->getAll('athlete/activities');

			$activities = [];
			foreach ($list as $activity) {
				if($activity->type == 'Ride') {
					$act_details = $this->api->get('activities/' . $activity->id);
					$activities[] = $act_details;
				}
			}

			$this->write('activities', $activities);
		}

	}

	/**
	 * Check if user have new activities
	 *  If number of activities on the platform is bigger than the number in cache
	 * 		then find new activities and download the details
	 *  
	 */
	public function refreshCache() {
		

		// Récupération des stats de l'utilisateur
		$stats = $this->api->get('athletes/'.$this->user->getUser()->getSocialId().'/stats');
		$activitiesCount = $stats->all_ride_totals->count;

		// Récupération de la liste des activités de l'utilisateur
		$list = $this->getAll('athlete/activities');

		// Récupération des ids des activités en cache
		$activities_ids = array_keys($this->get('activities')['activities']);

		$activities = [];
		foreach ($list as $activity) {
			if($activity->type == 'Ride' && 
				!in_array($activity->id, $activities_ids)) {
				$activities[] = $this->api->get('activities/' . $activity->id);
			}
		}
		$this->append('activities', $activities);

	}

	public function clearCache($forUserOnly = true) {

		if( !$forUserOnly ) {
			$files = glob($this->cacheFolder);
		} else {
			$files = glob($this->cacheFolder.$this->user->getUser()->getId().'_*');
		}

		foreach($files as $f) {
			if(!unlink($f)) {
				return false;
			}
		}

		return true;

	}



	/**
	 * Read cache file
	 */
	protected function read($key) {
		return json_decode(file_get_contents($this->cacheFolder.$this->user->getUser()->getId().'_'.$key));
	}

	/**
	 * Write cache file
	 */
	protected function write($key, $data) {
		file_put_contents($this->cacheFolder.$this->user->getUser()->getId().'_'.$key, json_encode($data));
	}

	/**
	 * Append $data to $key cache file
	 */
	protected function append($key, $data) {
		$cached = $this->read($key);
		$cached = array_merge($cached, $data);
		$this->write($key, $cached);
	}

	/**
	 * Check if the file userid_$key is valid
	 */
	protected function isCacheValid($key) {
		$f = $this->cacheFolder.$this->user->getUser()->getId().'_'.$key;
		return file_exists($f) && (filemtime($f) + $this->cacheLifetime >= time());
	}


	/** 
     * This function allow to retrieve all objects from paginated
     * source.
     * 
     */
	protected function getAll($request, $parameters = array()) {
		
		$activities = [];
		$page = 1;

		do {
			$requestParams = array_merge($parameters, ['page'=>$page, 'per_page'=>CacheStrava::MAX_PER_PAGE]);
			$res = $this->api->get($request, $requestParams);
			if(is_array($res)) {
				$activities = array_merge($activities, $res);
				++$page;
			} else {
				break;
			}

		} while( count($res) == CacheStrava::MAX_PER_PAGE );
		
		return $activities;

	}


	/** 
	 * Index the array with values ids
	 */
	private function indexify($items) {

		$arr = [];
		foreach ($items as $key => $item) {
			$arr[$item->id] = $item;
		}
		return $arr;

	}

}
