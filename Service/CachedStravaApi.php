<?php

namespace SylvainLG\Bundle\StravaCacheBundle\Service;

use Iamstuartwilson\StravaApi;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CachedStravaApi extends StravaApi {

	const MAX_PER_PAGE = 199;
	// const MAX_PER_PAGE = 2;

	private $user;
	private $api;
	private $params;

	private $cacheFilename;
	private $invalidatedCache = false;
	private $cacheFolder;
	private $cacheLifetime;

	private $cacheEnabled = true;

	public function __construct(TokenStorage $ts, $params, $cacheFolder, $cacheTime) {

    	$this->tokenStorage = $sc;
    	$this->user = $ts->getToken()->getUser();
    	$this->params = $params;

    	$this->cacheFolder = $cacheFolder;
    	$this->cacheLifetime = intval($cacheTime);

    	parent::__construct($this->params['strava']['client_id'], $this->params['strava']['client_secret']);
    	$this->setAccessToken($this->tokenStorage->getToken()->getAccessToken());

	}

	/*
	 * { inheritedDoc }
	 */
	public function get($request, $parameters = array(), $forceInvalidate = false) {

		$this->invalidatedCache = $forceInvalidate;

		$parameters = $this->generateParameters($parameters);
		$requestUrl = $this->parseGet($this->apiUrl . $request, $parameters);

		$this->cacheFilename = $this->user->getUser()->getId().'_'.sha1($requestUrl);
		
		if($this->cacheEnabled && $this->isCacheValid()) {
			return json_decode(file_get_contents($this->cacheFolder.$this->cacheFilename));
		}

		$res = $this->request($requestUrl);

		if($this->cacheEnabled) {
			file_put_contents($this->cacheFolder.$this->cacheFilename, json_encode($res));
		}
		$this->invalidatedCache = false;

		return $res;
	}

	public function put($request, $parameters = array()) {
		$this->invalidatedCache = true;
		return parent::put($request, $parameters);
	}

	public function post($request, $parameters = array()) {
		$this->invalidatedCache = true;
		return parent::post($request, $parameters);
	}

	public function delete($request, $parameters = array()) {
		$this->invalidatedCache = true;
		return parent::delete($request, $parameters);
	}

	protected function isCacheValid() {

		return (!$this->invalidatedCache) &&
			 file_exists($this->cacheFolder.$this->cacheFilename) && 
			 (filemtime($this->cacheFolder.$this->cacheFilename) + $this->cacheLifetime >= time());
	}



	public function clearCache($forUserOnly = true) {

		if( !$forUserOnly ) {
			$files = glob($this->cacheFolder);
		} else {
			$files = glob($this->cacheFolder.$this->user->getUser()->getId().'_*');
		}

		foreach($files as $f) {
			if(!unlink($f)) {
				return false;
			}
		}

		return true;

	}

	public function countCachedObjectForUser() {

		$i = 0;

		$toGlob = str_replace('/', DIRECTORY_SEPARATOR, $this->cacheFolder.$this->user->getUser()->getId().'_*');

		foreach( glob($this->cacheFolder.$this->user->getUser()->getId().'_*') as $file ) {
			$content = file_get_contents($file);
			$json = json_decode($content);

			$i += is_array($json) ? count($json) : 1;

		}

		return $i;

	}


	protected function bypassCache($bool) {
		$this->cacheEnabled = !$bool;
	}

	/** 
     * This function allow to retrieve all objects from paginated
     * source.
     * 
     */
	public function getAll($request, $parameters = array()) {
		
		$this->invalidatedCache = false;

		$fakeRequestUrl = $this->parseGet($this->apiUrl . $request, array_merge($parameters, ['per_page'=>'inf']));
		$fakeFilename = $this->cacheFilename = $this->user->getUser()->getId().'_'.sha1($fakeRequestUrl);

		if($this->isCacheValid()) {
			return json_decode(file_get_contents($this->cacheFolder.$fakeFilename));
		}


		// If the complete list is not in cache,
		// build it !
		$this->bypassCache(true);

		$activities = [];
		$page = 1;

		do {
			$requestParams = array_merge($parameters, ['page'=>$page, 'per_page'=>CachedStravaApi::MAX_PER_PAGE]);
			$res = $this->get($request, $requestParams);
			if(is_array($res)) {
				$activities = array_merge($activities, $res);
				++$page;
			} else {
				break;
			}

		} while( count($res) == CachedStravaApi::MAX_PER_PAGE );

		$this->bypassCache(false);
		
		file_put_contents($this->cacheFolder.$fakeFilename, json_encode($activities));

		return $activities;

	}

	public function getCache() {

		
		
		return [];
	}

}
